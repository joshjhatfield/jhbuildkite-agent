#!/bin/bash

# inits the buildkite container


Runbuildkite() {
	/root/.buildkite-agent/bin/buildkite-agent start
}

Envsubst() {
	envsubst < /root/buildkite-agent.cfg > /root/.buildkite-agent/buildkite-agent.cfg
	envsubst < /root/environment > /root/.buildkite-agent/hooks/environment
	envsubst < /root/pre-checkout > /root/.buildkite-agent/hooks/pre-checkout
	mkdir /root/.aws && envsubst < /root/awscli.conf > /root/.aws/config
}


Envsubst
Runbuildkite


while true
do
    sleep 1
done