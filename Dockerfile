FROM alpine:3.7

RUN apk update

RUN apk add bash curl gettext python2 py-pip make docker

RUN pip install awscli --upgrade --user

RUN pip install docker-compose

RUN ln -s /root/.local/bin/aws /usr/local/bin/aws

RUN /bin/bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`"

COPY files/environment /root/environment

COPY files/pre-checkout /root/pre-checkout

COPY files/buildkite-agent.cfg /root/

COPY files/awscli.conf /root/

COPY files/runcontainer.sh /root/

CMD bash /root/runcontainer.sh